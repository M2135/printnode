from os.path import exists
from requests.auth import HTTPBasicAuth

import os
import requests
import json

config_file = 'config.json'
job_dir = 'jobs'

DEBUG = True

def write_config_file(config):
  if DEBUG: print("__write_config_file")
  if not config:
    config = {
      "host" : "http://localhost:3000",
      "url_api_key": "/api/v1/printnodes",
      "url_printjobs": "/api/v1/printjobs",
      "api_key" : "",
      "ports" : []
    }
    
  config_json = json.dumps(config, indent = 4)
  with open(config_file, "w") as outfile:
    outfile.write(config_json)

def read_config():
  if DEBUG: print("__read_config")
  if not exists(config_file): write_config_file("")

  with open('config.json', 'r') as c_file:
    data=c_file.read()
    c = json.loads(data)
    return c

def read_serial_number():
  if DEBUG: print("__read_serial_number")
  if not exists('serial_number.txt'):
    print("File serial_number.txt does not exist")
    exit()
  with open('serial_number.txt', 'r') as file:
    return file.read().strip()


def get_api_key(config):
  if DEBUG: print("__get_api_key")
  sn = read_serial_number()
  if not sn:
    print("No serial number")
    exit()

  headers = {"Content-Type": "application/json; charset=utf-8;", "sn": sn}
  url = config['host'] + config['url_api_key']
  
  response = requests.post(url, headers=headers)
  data = response.json()

  if response.status_code == 200 and data['api_key']: return data['api_key']
  else: return ""

def get_printjobs(config):
  if DEBUG: print('__get_print_jobs')
  headers = {"Content-Type": "application/json; charset=utf-8;", "api_key": config['api_key']}
  url = config['host'] + config['url_printjobs']
  
  response = requests.get(url, headers=headers)
  data = response.json()  

  if response.status_code == 200 and data['results']:
    return data
  else:
    return ""

def delete_printjob(id, config):
  if DEBUG: print('__delete_print_job')
  headers = {"Content-Type": "application/json; charset=utf-8;", "api_key": config['api_key']}
  url = config['host'] + config['url_printjobs'] + '/' + str(id)

  response = requests.delete(url, headers=headers)
  data = response.json()


def save_printjobs(printjobs):
  if DEBUG: print('__save_print_jobs')
  if not os.path.exists(job_dir):
    os.makedirs(job_dir)

  for job in printjobs['results']:
 
    file_name = job['created_at'] + "." + str(job['id']) + "." + job['port'] + ".ezpl"
    file_path = job_dir + '/' + file_name
    
    with open(file_path, "w") as outfile:
      outfile.write(job['job'])

    # Convert newline char to unix
    os.system('dos2unix ' + file_path)
    # Add trailing newline (needed for lp to accept command 'E' (end)
    os.system('echo \'\n\' >> ' + file_path)
    print('Writing file: ' + file_path)


def process_printjobs(config):
  if DEBUG: print('__process_print_jobs')
  if os.path.exists(job_dir):
    for filename in os.listdir(job_dir):
      job_id = ""
      port = ""
    
      if filename.endswith(".ezpl"): 
        print(filename)
        x = filename.index('.')
        y = filename.index('.', x+1)
        z = filename.index('.', y+1)

        if z > y > x:
          job_id = filename[x+1:y]
          job_port = filename[y+1:z]
    
      if job_id and job_port:
        print("Printing job " + filename)
        os.system('lp -d ' + job_port + ' ' + job_dir + '/' + filename)
        print("Removing file " + filename)
        os.system('rm ' + job_dir + '/' + filename)
        delete_printjob(job_id, config)
      
def main():
  config = read_config()

  if not config['api_key']:
    config['api_key'] = get_api_key(config)
    if config['api_key']: write_config_file(config)

  if not config['api_key']:
    print("Exit: Failed to get api key")
    exit()

  printjobs = get_printjobs(config)
  if printjobs: save_printjobs(printjobs)
        

  process_printjobs(config)


if __name__ == "__main__":
    main()
