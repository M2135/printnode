## Printnode setup

### In case of Raspberry PI OS:

* Prepare SD card with Raspberry PI OS Lite (no Desktop)
* Prepare file "wpa_supplicant.conf" with your WLAN settings
* Copy files "wpa_supplicant.conf" and "ssh" to boot-Partition of the SD card


```sh
sudo apt update
sudo apt upgrade
sudo apt install net-tools cups dos2unix git vim python3-pip python3-dev python3-rpi.gpio picap
pip3 install gpiozero
```

### Add path to .bashrc
```sh
sudo echo "export PATH="/home/pi/.local/bin:$PATH"*" >> ~/.bashrc
source ~/.bashrc
```

### Clone this repo
```sh
cd ~
git clone https://M2135@bitbucket.org/M2135/printnode.git
```

### Configure auto execution of button.py
* sudo vim /etc/rc.local
* Add before "exit 0":
```sh
cd /home/pi/printnode
python3 button.py
```

### Change IP binding of cupsd
```sh
sudo vim /etc/cups/cupsd.conf
```
Change **Listen localhost:631** to **Listen 0.0.0.0:631**
```sh
sudo systemctl start cups
sudo systemctl status cups
```

### Add printer queue
```sh
sudo lpinfo -v
Example fpr connected printer: direct usb://GODEX/RT200?serial=192500F2

sudo lpadmin -p godex_1 -E -v usb://GODEX/RT200?serial=192500F2 -m raw -o usb-unidir-default=true,usb-no-reattach-default=true
sudo lpstat -v
cd ~/printnode
lp -d godex_1 test.ezpl
```

#### Remove printer queue (if needed)
```sh
sudo lpadmin -x godex_1
```

### Example EZPL file
```sh
^L
AB,105,5,0,0,0,0,JD 2021-08-05 PFA
W155,37,3,2,L,8,2,32,1
labsamples.io/s/4397222822846f58
AB,165,35,0,0,0,0,ID 1234567890
AB,165,60,0,0,0,0,M.gastr. ipsi right
AB,105,90,0,0,0,0,SISN1T3 178
E
```

### Print job example description
* ^L -> begin
* AB,105,5,0,0,0,0 -> Text size:B at Position x:105, y:5, Letters width/height/stretch/...:0, Text to print: JD 2021-08-05 PFA
* W -> QR code
* 155 -> x pos (upper right corner of QR code, because of 90° turned)
* 20 -> y pos (upper right corner of QR code, because of 90° turned)
* 3 -> mode (3 = 8-bit, supports Up/Down case)
* 2 -> qr code type (2 = enhanced qr code)
* L -> error correction (M = low)
* 8 -> masking factor (8 = auto)
* 2 -> multiple (=size * 2)
* 32 -> len (data = 32 chars)
* 1 -> rotation (1 = 90 degree)
* labsamples.io/s/4397222822846f58 -> data of qr code
* Other AB lines: see first line
* E -> end
* **Important** -> After last E in file a trailing newline is needed!


### Serial number
```sh
sudo vim ~/printnode/serial_number.txt
```
Generate serial number in web interface and replace the number in file serial_number.txt

### Config Eth0
```sh
sudo mv /etc/dhcpcd.conf /etc/dhcpcd.conf.org
sudo cp ~/printnode/dhcpcd.conf /etc/
```

### Test
```sh
python button.py
```
Press the button :-)